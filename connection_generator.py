import psycopg

profile = {
    "dbname":"zr_cs",
    "user":"reader",
    "password":"reader",
    "host":"localhost",
    "port":"5432"
    }


class Connection_Generator:
    
    def get_new_connection(self, profile):
        try:
            conn = psycopg.connect(
                    dbname = profile.get("dbname"),
                    user = profile.get("user"),
                    password = profile.get("password"),
                    host = profile.get("host"),
                    port = profile.get("port")
                )
            return conn
        except Exception as e:
            print("Dabase connection error: ", e)

    def release_connection(self, conn):
        conn.close()




