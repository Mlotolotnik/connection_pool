import psycopg
import threading

profile = {
    "dbname":"zr_cs",
    "user":"reader",
    "password":"reader",
    "host":"localhost",
    "port":"5432"
    }


class Connection_Pool:
    connection_pool = []
    lock = threading.Lock()
    
    def __init__(self, max_conn_limit=90, free_conn_limit=10, refresh_pool_interval=4, refresh_pool_counter=0):
        self.max_conn_limit = max_conn_limit
        self.free_conn_limit = free_conn_limit
        self.refresh_pool_interval = refresh_pool_interval
        self.refresh_pool_counter = refresh_pool_counter
        for i in range(free_conn_limit):
            self.add_connection_to_pool(profile)
    
    def add_connection_to_pool(self, profile):
        if len(self.connection_pool) >= self.max_conn_limit:
            print(f"Error. max_conn_limit reached: {self.max_conn_limit}")
            return None
        else:
            try:
                conn = psycopg.connect(
                        dbname = profile.get("dbname"),
                        user = profile.get("user"),
                        password = profile.get("password"),
                        host = profile.get("host"),
                        port = profile.get("port")
                    )
                self.connection_pool.append([conn, "free"])
                print("Connection created.")
                return conn
            except Exception as e:
                print("Dabase connection error: ", e)

    def pick_up_conn(self):
        with self.lock:
            self.refresh_pool_counter += 1
            if self.refresh_pool_counter >= self.refresh_pool_interval:
                self.clean_up_pool()
            for i in range(len(self.connection_pool)):
                if self.connection_pool[i][1] == "free":
                    self.connection_pool[i][1] = "busy"
                    return self.connection_pool[i][0]
            try:
                new_conn = self.add_connection_to_pool(profile)
                new_conn[1] = "busy"
                return new_conn
            except:
                print("Unable to create, a new connection.")
                return None
                
    def put_back_conn(self, conn):
        with self.lock:
            self.refresh_pool_counter += 1
            if self.refresh_pool_counter >= self.refresh_pool_interval:
                self.clean_up_pool()
            for i in range(len(self.connection_pool)):
                if self.connection_pool[i][0] is conn:
                    self.connection_pool[i][1] = "free"
            

    def clean_up_pool(self):
        self.refresh_pool_counter = 0
        print("Refreshing connection pool.")
        busy_conn = 0
        free_conn = 0

        for i in self.connection_pool:
            if i[1] == "free":
                free_conn += 1
            elif i[1] == "busy":
                busy_conn += 1
            
        if free_conn > self.free_conn_limit:
            print("Closing excessive connections...")
            num_conn_to_delete = free_conn - self.free_conn_limit
            for i in self.connection_pool:
                if i[1] == "free":
                    i[0].close
                    self.connection_pool.remove(i)
                    num_conn_to_delete -= 1
                    if num_conn_to_delete == 0:
                        break
            print("Pool refreshed.")
        if free_conn < self.free_conn_limit:
            print("Creating missing connections...")
            num_conn_to_create = self.free_conn_limit - free_conn
            for i in range(num_conn_to_create):
                if len(self.connection_pool) < self.max_conn_limit:
                    num_conn_to_create -= 1
                    self.add_connection_to_pool(profile)
                    if num_conn_to_create == 0:
                        break
            print("Pool refreshed.")

                    
    def info(self):
        with self.lock:
            busy_conn = 0
            free_conn = 0
            for i in self.connection_pool:
                if i[1] == "free":
                    free_conn += 1
                elif i[1] == "busy":
                    busy_conn += 1
            print(f"Total number of connections: {len(self.connection_pool)}")
            print(f"Total connections limit: {self.max_conn_limit}")
            print(f"Free connections: {free_conn}")
            print(f"Free connections limit: {self.free_conn_limit}")
            print(f"Busy connections: {busy_conn}")



my_conn = []
print("===Creating connection pool...===")
con_pool = Connection_Pool()
print("===Connection pool created.===")
con_pool.info()
print("===Picking up connections===")
for i in range(50):
    my_conn.append(con_pool.pick_up_conn())
    print(f"Conn {i}, picked up.")
print("===Connections picked up.===")
con_pool.info()
print("===Putting back connection...===")
for i in range(49, -1, -1):
    print(f"Putting back conn {i}...")
    con_pool.put_back_conn(my_conn[i])
    print(f"Conn {i}, put back.") 
print("===Connections put back.===")
con_pool.info()
print("===EXITed===")



